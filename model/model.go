package model

import (
	"gorm.io/gorm"
	"nav/utils"
	"time"
)

type BaseModel struct {
	ID        string     `gorm:"primary_key'" json:"id"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

// 生成全局唯一ID
func (m *BaseModel) BeforeCreate(tx *gorm.DB) (err error) {
	m.ID = utils.UniqueId()
	return
}